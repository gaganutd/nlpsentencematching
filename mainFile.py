# -*- coding: utf-8 -*-
"""
Created on Fri Nov 22 21:37:08 2019

@author: gajossan
"""

"""
Created on Thu Oct 31 20:29:12 2019

@author: maitr
"""

import pandas as pd
df=pd.read_csv('C:/Users/gajossan/Desktop/NLP/PROJECT/data/sample_train.txt', sep="\t",error_bad_lines=False)
print(df)

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk import pos_tag
df.columns=["id","Sentence1","Sentence2","Gold Tag"]
df_features=df.copy()
#df_features.columns=["id","Sentence1","Sentence2","Gold Tag"]

from nltk.corpus import wordnet as wn

def is_noun(tag):
    return tag in ['NN', 'NNS', 'NNP', 'NNPS']


def is_verb(tag):
    return tag in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']


def is_adverb(tag):
    return tag in ['RB', 'RBR', 'RBS']


def is_adjective(tag):
    return tag in ['JJ', 'JJR', 'JJS']


def penn_to_wn(tag):
    if is_adjective(tag):
        return wn.ADJ
    elif is_noun(tag):
        return wn.NOUN
    elif is_adverb(tag):
        return wn.ADV
    elif is_verb(tag):
        return wn.VERB
    return 'n'

#Tokenize both sentences into words
df_features["Token_Sentence1"] = df["Sentence1"].apply(word_tokenize)
df_features["Token_Sentence2"] = df["Sentence2"].apply(word_tokenize)

#lemmatization

from nltk.stem.wordnet import WordNetLemmatizer



postag_Sentence1=[]
postag_Sentence2=[]
for index, row in df_features.iterrows():
    sen1temp=[]
    sen2temp=[]
    for i in row['Token_Sentence1']:
        sen1temp.append(i)
    for j in row['Token_Sentence2']:
        sen2temp.append(j)
    postag_Sentence1.append(pos_tag(sen1temp))
    postag_Sentence2.append(pos_tag(sen2temp))
    
df_features["PosTag_Sentence1"] = postag_Sentence1
df_features["PosTag_Sentence2"] = postag_Sentence2  
    
lemmatizer = WordNetLemmatizer()
lemmatized_Sentence1=[]
lemmatized_Sentence2=[]

for index, row in df_features.iterrows():
    sen1temp=[]
    sen2temp=[]
    for idx,i in enumerate(row['Token_Sentence1']):
#        print(penn_to_wn(df_features['PosTag_Sentence1'][index][idx][1]))
        w1 =lemmatizer.lemmatize(i,penn_to_wn(df_features['PosTag_Sentence1'][index][idx][1]))
        sen1temp.append(w1)
    for idx,j in enumerate(row['Token_Sentence2']):
#        print(penn_to_wn(df_features['PosTag_Sentence2'][index][idx][1]))
        w2 =lemmatizer.lemmatize(j,penn_to_wn(df_features['PosTag_Sentence2'][index][idx][1]))
        sen2temp.append(w2)
    lemmatized_Sentence1.append(sen1temp)
    lemmatized_Sentence2.append(sen2temp)



df_features["Lema_Sentence1"] = lemmatized_Sentence1
df_features["Lema_Sentence2"] = lemmatized_Sentence2  

    


from pycorenlp import StanfordCoreNLP
nlp = StanfordCoreNLP('http://localhost:9000')


from nltk.parse.stanford import StanfordDependencyParser
path_to_jar = 'C://Users//gajossan//Desktop//NLP//stanford-parser-full-2018-10-17//stanford-parser.jar'
path_to_models_jar = 'C://Users//gajossan//Desktop//NLP//stanford-parser-full-2018-10-17//stanford-parser-3.9.2-models.jar'
dependency_parser = StanfordDependencyParser(path_to_jar=path_to_jar, path_to_models_jar=path_to_models_jar)

sent1_Synt_ParseTree = []
sent2_Synt_ParseTree = []

sent1_Dep_ParseTree = []
sent2_Dep_ParseTree = []


for index, row in df_features.iterrows():
    output1 = nlp.annotate(row['Sentence1'], properties={
      'annotators': 'parse',
      'outputFormat': 'json'
    })
    output2 = nlp.annotate(row['Sentence2'], properties={
      'annotators': 'parse',
      'outputFormat': 'json'
    })
    sent1_Synt_ParseTree.append(output1['sentences'][0]['parse'])
    sent2_Synt_ParseTree.append(output2['sentences'][0]['parse'])
    sent1_Dep_ParseTree.append(next(dependency_parser.raw_parse(row['Sentence1'])).triples())
    sent2_Dep_ParseTree.append(next(dependency_parser.raw_parse(row['Sentence2'])).triples())
  
df_features['Synt_ParseTree_S1'] = sent1_Synt_ParseTree
df_features['Synt_ParseTree_S2'] = sent2_Synt_ParseTree

df_features['Depend_ParseTree_S1'] = sent1_Dep_ParseTree
df_features['Depend_ParseTree_S2'] = sent2_Dep_ParseTree


listOfHypernymSent1 = []
listOfHypernymSent2 = []

listOfHyponymSent1 = []
listOfHyponymSent2 = []

listOfHolonymSent1 = []
listOfHolonymSent2 = []

listOfMeronymSent1 = []
listOfMeronymSent2 = []

from nltk.corpus import wordnet

for index, row in df_features.iterrows():
    listOfWordsOfSen1sHyper=[]
    listOfWordsOfSen1sHypo=[]
    listOfWordsOfSen1sHolo=[]
    listOfWordsOfSen1sMero=[]
    for word in row['Token_Sentence1']:
        wordDictHyper = {}
        wordDictHypo = {}
        wordDictHolo = {}
        wordDictMero = {}
        
        listOfSSHypePair = []
        listOfSSHypoPair = []
        listOfSSHoloPair = []
        listOfSSMeroPair = []
        
        for ss in wordnet.synsets(word):
            listOfSSHypePair.append((ss,ss.hypernyms()))
            listOfSSHypoPair.append((ss,ss.hyponyms()))
            listOfSSHoloPair.append((ss,ss.part_holonyms()))
            listOfSSMeroPair.append((ss,ss.part_meronyms()))
            
        wordDictHyper[word] = listOfSSHypePair
        wordDictHypo[word] = listOfSSHypoPair
        wordDictHolo[word] = listOfSSHoloPair
        wordDictMero[word] = listOfSSMeroPair
        
        listOfWordsOfSen1sHyper.append(wordDictHyper)
        listOfWordsOfSen1sHypo.append(wordDictHypo)
        listOfWordsOfSen1sHolo.append(wordDictHolo)
        listOfWordsOfSen1sMero.append(wordDictMero)
    listOfHypernymSent1.append(listOfWordsOfSen1sHyper)  
    listOfHyponymSent1.append(listOfWordsOfSen1sHypo)  
    listOfHolonymSent1.append(listOfWordsOfSen1sHolo)  
    listOfMeronymSent1.append(listOfWordsOfSen1sMero)  
    
    
    listOfWordsOfSen2sHyper=[]
    listOfWordsOfSen2sHypo=[]
    listOfWordsOfSen2sHolo=[]
    listOfWordsOfSen2sMero=[]
    for word in row['Token_Sentence2']:
        wordDictHyper = {}
        wordDictHypo = {}
        wordDictHolo = {}
        wordDictMero = {}
        
        listOfSSHypePair = []
        listOfSSHypoPair = []
        listOfSSHoloPair = []
        listOfSSMeroPair = []
        
        for ss in wordnet.synsets(word):
            listOfSSHypePair.append((ss,ss.hypernyms()))
            listOfSSHypoPair.append((ss,ss.hyponyms()))
            listOfSSHoloPair.append((ss,ss.part_holonyms()))
            listOfSSMeroPair.append((ss,ss.part_meronyms()))
            
        wordDictHyper[word] = listOfSSHypePair
        wordDictHypo[word] = listOfSSHypoPair
        wordDictHolo[word] = listOfSSHoloPair
        wordDictMero[word] = listOfSSMeroPair
        
        listOfWordsOfSen2sHyper.append(wordDictHyper)
        listOfWordsOfSen2sHypo.append(wordDictHypo)
        listOfWordsOfSen2sHolo.append(wordDictHolo)
        listOfWordsOfSen2sMero.append(wordDictMero)
    listOfHypernymSent2.append(listOfWordsOfSen2sHyper)  
    listOfHyponymSent2.append(listOfWordsOfSen2sHypo)  
    listOfHolonymSent2.append(listOfWordsOfSen2sHolo)  
    listOfMeronymSent2.append(listOfWordsOfSen2sMero)
    
df_features['Hyper_Sentence1'] = listOfHypernymSent1
df_features['Hyper_Sentence2'] = listOfHypernymSent2

df_features['Hypo_Sentence1'] = listOfHyponymSent1
df_features['Hypo_Sentence2'] = listOfHyponymSent2

df_features['Holo_Sentence1'] = listOfHolonymSent1
df_features['Holo_Sentence2'] = listOfHolonymSent2

df_features['Mero_Sentence1'] = listOfMeronymSent1
df_features['Mero_Sentence2'] = listOfMeronymSent2


import spacy
nlp = spacy.load('en')
import networkx as nx
deparr1=[]
deparr2=[]
for index, row in df_features.iterrows():
    
    G = nx.DiGraph()
    dep1temp=[]
    doc=nlp(row['Sentence1'])
    for token in doc:
        if(token.head.text == token.text):
            root = token.text
        dep1temp.append((token.head.text, token.text))
    G.add_edges_from(dep1temp)
    paths1=[]
    for node in G:
        if G.out_degree(node)==0: #it's a leaf
             paths1.append(nx.shortest_path(G, root, node))
    deparr1.append(paths1)

    G = nx.DiGraph()
    dep2temp=[] #stores all edges(word pairs) of graph of sentence
    doc=nlp(row['Sentence2'])
    for token in doc:
        if(token.head.text == token.text):
            root = token.text
        dep2temp.append((token.head.text, token.text))
    G.add_edges_from(dep2temp) # make graph from edges
    paths2=[]    # stores paths 
    for node in G:
        if G.out_degree(node)==0: #it's a leaf
            paths2.append(nx.shortest_path(G, root, node))
    deparr2.append(paths2)   # all paths of that sentence starting from root

    

df_features["DepTree_Sentence1"] = deparr1
df_features["DepTree_Sentence2"] = deparr2 













