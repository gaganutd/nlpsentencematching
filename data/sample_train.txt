﻿id	Sentence1	Sentence2	Gold Tag

s_1	Nationally, the federal Centers for Disease Control and Prevention recorded 4,156 cases of West Nile, including 284 deaths.	There were 293 human cases of West Nile in Indiana in 2002, including 11 deaths statewide.	2
s_2	Micron has declared its first quarterly profit for three years.	Micron's numbers also marked the first quarterly profit in three years for the DRAM manufacturer.	4
s_3	The fines are part of failed Republican efforts to force or entice the Democrats to return.	Perry said he backs the Senate's efforts, including the fines, to force the Democrats to return.	3
s_4	The American Anglican Council, which represents Episcopalian conservatives, said it will seek authorization to create a separate group.	The American Anglican Council, which represents Episcopalian conservatives, said it will seek authorization to create a separate province in North America because of last week's actions.	3
s_5	John rules the lake.	The lake is being ruled by John.	4
